package de.mekaso.ejb;

import java.math.BigDecimal;
import java.math.RoundingMode;

import javax.ejb.Local;
import javax.ejb.Stateless;
import javax.inject.Inject;

import de.mekaso.ejb.exceptions.DoesNotWorkException;
import de.mekaso.ejb.model.Operation;
import de.mekaso.ejb.service.CalculationService;
import de.mekaso.ejb.stat.OperationService;

@Local(CalculationService.class)
@Stateless(name = "CalculationService")
public class CalculationServiceLocal implements CalculationService {

	@Inject
	private OperationService operationService;
	
	@Override
	public BigDecimal calculate(Operation operation) throws DoesNotWorkException {
		BigDecimal result = null;
		switch (operation.getOperationType()) {
		case Addition:
			result = add(operation);
			break;
		case Subtraction:
			result = subtract(operation);
			break;
		case Multiplication:
			result = multiply(operation);
			break;
		case Division:
			result = divide(operation);
			break;
		default:
			break;
		}
		this.operationService.saveOperation(operation);
		return result;
	}

	private BigDecimal add(Operation operation) {
		return operation.getA().add(operation.getB());
	}

	private BigDecimal subtract(Operation operation) {
		return operation.getA().subtract(operation.getB());
	}

	private BigDecimal multiply(Operation operation) {
		return operation.getA().multiply(operation.getB());
	}

	private BigDecimal divide(Operation operation) throws DoesNotWorkException {
		if (operation.getB().equals(BigDecimal.ZERO) || operation.getB().doubleValue() == 0.0) {
			throw new DoesNotWorkException("You cannot divide by ZERO.");
		}
		return operation.getA().divide(operation.getB(), 4, RoundingMode.CEILING);
	}

	@Override
	public Integer numberOfCalculations() {
		return this.operationService.countOperations();
	}
}
