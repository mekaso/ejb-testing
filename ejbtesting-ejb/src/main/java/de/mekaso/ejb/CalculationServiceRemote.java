package de.mekaso.ejb;

import java.math.BigDecimal;

import javax.ejb.Remote;
import javax.ejb.Stateless;

import de.mekaso.ejb.exceptions.DoesNotWorkException;
import de.mekaso.ejb.model.Operation;
import de.mekaso.ejb.service.CalculationService;

@Remote(CalculationService.class)
@Stateless
public class CalculationServiceRemote extends CalculationServiceLocal {

	@Override
	public BigDecimal calculate(Operation operation) throws DoesNotWorkException {
		return super.calculate(operation);
	}
}
