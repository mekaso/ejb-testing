package de.mekaso.ejb.stat;

import de.mekaso.ejb.model.Operation;

public interface OperationService {
	public void saveOperation(Operation operation);
	public Integer countOperations();
}
