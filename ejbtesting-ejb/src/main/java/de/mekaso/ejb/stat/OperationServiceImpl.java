package de.mekaso.ejb.stat;

import java.util.List;

import javax.enterprise.context.ApplicationScoped;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.Transactional;

import de.mekaso.ejb.OperationEntity;
import de.mekaso.ejb.model.Operation;

@ApplicationScoped
@Transactional
public class OperationServiceImpl implements OperationService {

	@PersistenceContext
	private EntityManager entityManager;
	
	@Override
	public void saveOperation(Operation operation) {
		this.entityManager.persist(new OperationEntity(operation));
	}

	@Override
	@Transactional(value = Transactional.TxType.SUPPORTS)
	public Integer countOperations() {
		List<OperationEntity> results = this.entityManager.createNamedQuery("OperationEntity.getAll", OperationEntity.class).getResultList();
		return results == null ? Integer.valueOf(0) : results.size();
	}

}
