package de.mekaso.ejb;

import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

import de.mekaso.ejb.model.Operation;
import de.mekaso.ejb.model.OperationType;

@Entity
@Table
@NamedQuery(name = "OperationEntity.getAll", query = "SELECT t FROM OperationEntity t")
public class OperationEntity {
	@Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;
	@Column
	private BigDecimal a;
	@Column
	private BigDecimal b;
	@Column
	private OperationType operationType;
	public OperationEntity() {	}
	
	public OperationEntity(Operation operation) {
		this.a = operation.getA();
		this.b = operation.getB();
		this.operationType = operation.getOperationType();
	}

	/**
	 * @return the a
	 */
	public BigDecimal getA() {
		return a;
	}

	/**
	 * @param a the a to set
	 */
	public void setA(BigDecimal a) {
		this.a = a;
	}

	/**
	 * @return the b
	 */
	public BigDecimal getB() {
		return b;
	}

	/**
	 * @param b the b to set
	 */
	public void setB(BigDecimal b) {
		this.b = b;
	}

	/**
	 * @return the operationType
	 */
	public OperationType getOperationType() {
		return operationType;
	}

	/**
	 * @param operationType the operationType to set
	 */
	public void setOperationType(OperationType operationType) {
		this.operationType = operationType;
	}

	/**
	 * @return the id
	 */
	public int getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(int id) {
		this.id = id;
	}
}
