package de.mekaso.ejb.web;

import java.math.BigDecimal;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.enterprise.context.SessionScoped;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.inject.Named;

import de.mekaso.ejb.exceptions.DoesNotWorkException;
import de.mekaso.ejb.model.Operation;
import de.mekaso.ejb.model.OperationType;
import de.mekaso.ejb.service.CalculationService;

@Named
@SessionScoped
public class Index implements java.io.Serializable {
	private static final long serialVersionUID = 1L;
	private BigDecimal a;
	private BigDecimal b;
	private OperationType operationType;
	private OperationType [] operationTypes;
	private BigDecimal result;

	
	@EJB(beanName="CalculationService", beanInterface = CalculationService.class)
	private CalculationService calculationService;
	
	@PostConstruct
	public void init() {
		this.operationTypes = OperationType.values();
	}
	
	public void calculate() {
		Operation operation = new Operation(this.a, this.b, this.operationType);
		try {
			this.result = this.calculationService.calculate(operation);
		} catch (DoesNotWorkException e) {
			this.result = null;
			FacesMessage message = new FacesMessage();
			message.setSeverity(FacesMessage.SEVERITY_ERROR);
			message.setSummary(e.getMessage());
			FacesContext.getCurrentInstance().addMessage("calcform:b", message);
		}
	}
	
	public void showStatistics() {
		Integer numberOfCalculations = this.calculationService.numberOfCalculations();
		FacesMessage message = new FacesMessage();
		message.setSeverity(FacesMessage.SEVERITY_INFO);
		message.setSummary(String.format("I counted %s operations.", numberOfCalculations));
		FacesContext.getCurrentInstance().addMessage(null, message);
	}
	
	/**
	 * @return the a
	 */
	public BigDecimal getA() {
		return a;
	}
	/**
	 * @param a the a to set
	 */
	public void setA(BigDecimal a) {
		this.a = a;
	}
	/**
	 * @return the b
	 */
	public BigDecimal getB() {
		return b;
	}
	/**
	 * @param b the b to set
	 */
	public void setB(BigDecimal b) {
		this.b = b;
	}
	
	/**
	 * @return the operationType
	 */
	public OperationType getOperationType() {
		return operationType;
	}

	/**
	 * @param operationType the operationType to set
	 */
	public void setOperationType(OperationType operationType) {
		this.operationType = operationType;
	}

	/**
	 * @return the operationTypes
	 */
	public OperationType[] getOperationTypes() {
		return operationTypes;
	}

	/**
	 * @param operationTypes the operationTypes to set
	 */
	public void setOperationTypes(OperationType[] operationTypes) {
		this.operationTypes = operationTypes;
	}

	/**
	 * @return the result
	 */
	public BigDecimal getResult() {
		return result;
	}

	/**
	 * @param result the result to set
	 */
	public void setResult(BigDecimal result) {
		this.result = result;
	}
}
