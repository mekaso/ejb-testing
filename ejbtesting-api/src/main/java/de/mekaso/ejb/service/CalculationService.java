package de.mekaso.ejb.service;

import java.math.BigDecimal;

import de.mekaso.ejb.exceptions.DoesNotWorkException;
import de.mekaso.ejb.model.Operation;

public interface CalculationService {
	public BigDecimal calculate(Operation operation) throws DoesNotWorkException;
	public Integer numberOfCalculations();
}
