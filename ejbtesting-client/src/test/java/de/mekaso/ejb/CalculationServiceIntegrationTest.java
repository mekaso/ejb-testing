package de.mekaso.ejb;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import java.math.BigDecimal;

import javax.naming.NamingException;

import org.junit.After;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;

import de.mekaso.ejb.exceptions.DoesNotWorkException;
import de.mekaso.ejb.model.Operation;
import de.mekaso.ejb.model.OperationType;
import de.mekaso.ejb.service.CalculationService;

/**
 * This integration test methods will run if the application server is running and the ejb is deployed and running.
 */
@Ignore
public class CalculationServiceIntegrationTest {

	private CalculationService calculationService;
	
	@Before
	public void connect() throws NamingException {
		CalculationServiceClient serviceClient = new CalculationServiceClient("localhost", 8080);
		this.calculationService = serviceClient.connect();
		assertNotNull(calculationService);
	}
	
	@Test
	public void testAddition() throws DoesNotWorkException {
		Operation operation = new Operation(BigDecimal.ONE, BigDecimal.TEN, OperationType.Addition);
		BigDecimal result = this.calculationService.calculate(operation);
		assertEquals(BigDecimal.valueOf(11), result);
	}
	
	@Test
	public void testSubtraction() throws DoesNotWorkException {
		Operation operation = new Operation(BigDecimal.ONE, BigDecimal.TEN, OperationType.Subtraction);
		BigDecimal result = this.calculationService.calculate(operation);
		assertEquals(BigDecimal.valueOf(-9), result);
	}
	
	@Test
	public void testMultiplication() throws DoesNotWorkException {
		Operation operation = new Operation(BigDecimal.ONE, BigDecimal.TEN, OperationType.Multiplication);
		BigDecimal result = this.calculationService.calculate(operation);
		assertEquals(BigDecimal.TEN, result);
	}
	
	@Test
	public void testDivision() throws DoesNotWorkException {
		Operation operation = new Operation(BigDecimal.TEN, BigDecimal.TEN, OperationType.Division);
		BigDecimal result = this.calculationService.calculate(operation);
		assertEquals(BigDecimal.ONE, result);
	}
	
	@Test
	public void testDivisionByZero() {
		Operation operation = new Operation(BigDecimal.TEN, BigDecimal.ZERO, OperationType.Division);
		try {
			this.calculationService.calculate(operation);
		} catch (DoesNotWorkException ex) {
			assertEquals("You cannot divide by ZERO.", ex.getMessage());
		}
//		Throwable exception = assertThrows(DoesNotWorkException.class, () -> {
//			this.calculationService.calculate(operation);
//        });
	}
	
	@After
	public void disconnect() {
		this.calculationService = null;
	}
}
