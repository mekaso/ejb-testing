package de.mekaso.ejb;

import static org.junit.Assert.assertEquals;

import java.math.BigDecimal;

import org.jboss.weld.context.ejb.Ejb;
import org.jglue.cdiunit.AdditionalClasses;
import org.jglue.cdiunit.CdiRunner;
import org.jglue.cdiunit.InRequestScope;
import org.jglue.cdiunit.ejb.SupportEjb;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;

import de.mekaso.ejb.exceptions.DoesNotWorkException;
import de.mekaso.ejb.model.Operation;
import de.mekaso.ejb.model.OperationType;
import de.mekaso.ejb.service.CalculationService;
import de.mekaso.ejb.stat.OperationServiceImpl;
/**
 * This lightweight test will run without a application server, because deltaspike will start an ejb container for us.
 */
@RunWith(CdiRunner.class)
@SupportEjb
@AdditionalClasses({CalculationServiceLocal.class, OperationServiceImpl.class})
@Ignore
public class CalculationServiceLightweightTest {
	
	@Ejb
	private CalculationService calculationService;

	@Test
	@InRequestScope
	public void testAddition() throws DoesNotWorkException {
		Operation operation = new Operation(BigDecimal.ONE, BigDecimal.TEN, OperationType.Addition);
		BigDecimal result = this.calculationService.calculate(operation);
		assertEquals(BigDecimal.valueOf(11), result);
	}
	
	@Test
	public void testSubtraction() throws DoesNotWorkException {
		Operation operation = new Operation(BigDecimal.ONE, BigDecimal.TEN, OperationType.Subtraction);
		BigDecimal result = this.calculationService.calculate(operation);
		assertEquals(BigDecimal.valueOf(-9), result);
	}
	
	@Test
	public void testMultiplication() throws DoesNotWorkException {
		Operation operation = new Operation(BigDecimal.ONE, BigDecimal.TEN, OperationType.Multiplication);
		BigDecimal result = this.calculationService.calculate(operation);
		assertEquals(BigDecimal.TEN, result);
	}
	
	@Test
	public void testDivision() throws DoesNotWorkException {
		Operation operation = new Operation(BigDecimal.TEN, BigDecimal.TEN, OperationType.Division);
		BigDecimal result = this.calculationService.calculate(operation);
		assertEquals(BigDecimal.ONE, result);
	}
	
	@Test
	public void testDivisionByZero() {
		Operation operation = new Operation(BigDecimal.TEN, BigDecimal.ZERO, OperationType.Division);
		try {
			this.calculationService.calculate(operation);
		} catch (DoesNotWorkException ex) {
			assertEquals("You cannot divide by ZERO.", ex.getMessage());
		}
	}
	
	@Test
	public void testStatistic() throws DoesNotWorkException {
		int result = this.calculationService.numberOfCalculations();
		assertEquals(0, result);
	}
}
