package de.mekaso.ejb;

import java.util.Properties;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;

import de.mekaso.ejb.service.CalculationService;

public class CalculationServiceClient {
	/**
	 * The app name is the application name of the deployed EJBs. This is typically the ear name
     * without the .ear suffix. However, the application name could be overridden in the application.xml of the
     * EJB deployment on the server.
	 */
	private static final String APP_NAME = "ejbtesting";
	/**
	 * This is the module name of the deployed EJBs on the server. This is typically the jar name of the
	 * EJB deployment, without the .jar suffix, but can be overridden via the ejb-jar.xml
	 */
	private static final String MODULE_NAME = "ejbtest-module";
	/**
	 * AS7 allows each deployment to have an (optional) distinct name. We haven't specified a distinct name for
	 * our EJB deployment, so this is an empty string.
	 */
	private static final String DISTINCT_NAME = "";
	/**
	 * The EJB name which by default is the simple class name of the bean implementation class.
	 */
	private static final String REMOTE_BEAN_CLASS = "CalculationServiceRemote";
	/**
	 * the remote view fully qualified class name.
	 */
	private static final String REMOTE_INTERFACE = CalculationService.class.getName();
	/**
	 * the ejb server.
	 */
	private String server;
	/**
	 * the port number.
	 */
	private int port;

	public CalculationServiceClient() {	}
	
	public CalculationServiceClient(String server, int port) {
		this.server = server;
		this.port = port;
	}
	
	public CalculationService connect() throws NamingException {
		final Properties jndiProperties = new Properties();
		jndiProperties.put(Context.URL_PKG_PREFIXES, "org.jboss.ejb.client.naming");
		if (this.server != null) {
			jndiProperties.put("remote.connection.default.host", this.server);
		}
		if (this.port > 0) {
			jndiProperties.put("remote.connection.default.port", this.port);
		}
		Context ic = new InitialContext(jndiProperties);
        // let's do the lookup
		String lookUp = String.format("ejb:%s/%s/%s/%s!%s", APP_NAME, MODULE_NAME, DISTINCT_NAME, REMOTE_BEAN_CLASS, REMOTE_INTERFACE);
		System.out.println("Look at " + lookUp);
        CalculationService calculationService = (CalculationService) ic.lookup(lookUp);
		return calculationService;
	}
}