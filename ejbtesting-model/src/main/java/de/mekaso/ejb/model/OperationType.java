package de.mekaso.ejb.model;

public enum OperationType {
	Addition,
	Subtraction,
	Multiplication,
	Division
}
