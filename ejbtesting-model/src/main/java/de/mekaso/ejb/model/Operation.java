package de.mekaso.ejb.model;

import java.math.BigDecimal;

public class Operation implements java.io.Serializable {
	private static final long serialVersionUID = 1L;
	private BigDecimal a;
	private BigDecimal b;
	private OperationType operationType;

	public Operation(BigDecimal a, BigDecimal b, OperationType operationType) {
		this.a = a;
		this.b = b;
		this.operationType = operationType;
	}
	/**
	 * @return the a
	 */
	public BigDecimal getA() {
		return a;
	}
	/**
	 * @return the b
	 */
	public BigDecimal getB() {
		return b;
	}
	/**
	 * @return the operationType
	 */
	public OperationType getOperationType() {
		return operationType;
	}
}
