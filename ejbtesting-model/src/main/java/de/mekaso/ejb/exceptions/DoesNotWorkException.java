package de.mekaso.ejb.exceptions;

public class DoesNotWorkException extends Exception {
	private static final long serialVersionUID = 1L;

	public DoesNotWorkException() {
		super();
	}

	public DoesNotWorkException(String message) {
		super(message);
	}
}