package de.mekaso.vaadin;

import com.vaadin.icons.VaadinIcons;
import com.vaadin.ui.AbstractOrderedLayout;
import com.vaadin.ui.Alignment;
import com.vaadin.ui.Button;
import com.vaadin.ui.ComboBox;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Label;
import com.vaadin.ui.Layout.AlignmentHandler;
import com.vaadin.ui.TextField;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.themes.ValoTheme;

import de.mekaso.ejb.model.OperationType;

public class Layout {
    
    private boolean constructor;
    protected final VerticalLayout rootCSS = new VerticalLayout();
    protected final HorizontalLayout numbers = new HorizontalLayout();
    protected final TextField A = new TextField();
    protected final TextField B = new TextField();
    protected final ComboBox<OperationType> operationTypeComboBox = new ComboBox<>();
    protected final Button calculationButton = new Button();
    protected final Label resultLabel = new Label();
    
    public Layout() {
        super();
        constructor = true;
        reset();
        constructor = false;
    }
    
    
    public synchronized void reset() {
        rootCSS.removeAllComponents();
        numbers.addComponent(A);
        numbers.addComponent(B);
        numbers.addComponent(operationTypeComboBox);
        numbers.addComponent(calculationButton);
        rootCSS.addComponent(numbers);
        rootCSS.addComponent(resultLabel);
        initRootCSS();
        initResultLabel();
        initNumbers();
        initCalculationButton();
        initOperationCheckBoxGroup();
        initB();
        initA();
    }
    
    
    public final VerticalLayout getRootComponent() {
        return rootCSS;
    }
    
    protected void initA() {
        A.setValue("");
        A.setCaption("Zahl A");
        A.addStyleName("");
        A.setEnabled(true);
        A.setVisible(true);
        A.setReadOnly(false);
        A.setWidth("100.0%");
        A.setHeight("100.0%");
        A.setTabIndex(0);
        AbstractOrderedLayout parentAbstractOrderedLayout = (AbstractOrderedLayout) A.getParent();
        parentAbstractOrderedLayout.setExpandRatio(A, Double.valueOf(0.0).floatValue());
        AlignmentHandler parentAlignmentHandler = (AlignmentHandler) A.getParent();
        parentAlignmentHandler.setComponentAlignment(A, Alignment.TOP_LEFT);
    }
    
    
    public final TextField getA() {
        return A;
    }
    
    protected void initB() {
        B.setValue("");
        B.setCaption("Zahl B");
        B.addStyleName("");
        B.setEnabled(true);
        B.setVisible(true);
        B.setReadOnly(false);
        B.setWidth("100.0%");
        B.setHeight("100.0%");
        B.setTabIndex(0);
        AbstractOrderedLayout parentAbstractOrderedLayout = (AbstractOrderedLayout) B.getParent();
        parentAbstractOrderedLayout.setExpandRatio(B, Double.valueOf(0.0).floatValue());
        AlignmentHandler parentAlignmentHandler = (AlignmentHandler) B.getParent();
        parentAlignmentHandler.setComponentAlignment(B, Alignment.TOP_LEFT);
    }
    
    
    public final TextField getB() {
        return B;
    }
    
    protected void initOperationCheckBoxGroup() {
    	operationTypeComboBox.setCaption("Operation");
    	operationTypeComboBox.addStyleName("");
    	operationTypeComboBox.setEnabled(true);
    	operationTypeComboBox.setVisible(true);
    	operationTypeComboBox.setWidth("100.0%");
    	operationTypeComboBox.setHeight("100.0%");
    	operationTypeComboBox.setTabIndex(0);
        AbstractOrderedLayout parentAbstractOrderedLayout = (AbstractOrderedLayout) operationTypeComboBox.getParent();
        parentAbstractOrderedLayout.setExpandRatio(operationTypeComboBox, Double.valueOf(0.0).floatValue());
        AlignmentHandler parentAlignmentHandler = (AlignmentHandler) operationTypeComboBox.getParent();
        parentAlignmentHandler.setComponentAlignment(operationTypeComboBox, Alignment.TOP_LEFT);
    }
    
    
    public final ComboBox<OperationType> getOperationTypeComboBox() {
        return operationTypeComboBox;
    }
    
    protected void initCalculationButton() {
        calculationButton.setCaption("Calculate");
        calculationButton.addStyleName("");
        calculationButton.setEnabled(true);
        calculationButton.setVisible(true);
        calculationButton.setWidth("100.0%");
        calculationButton.setHeight("100.0%");
        calculationButton.setDisableOnClick(false);
        calculationButton.setTabIndex(0);
        AbstractOrderedLayout parentAbstractOrderedLayout = (AbstractOrderedLayout) calculationButton.getParent();
        parentAbstractOrderedLayout.setExpandRatio(calculationButton, Double.valueOf(0.0).floatValue());
        AlignmentHandler parentAlignmentHandler = (AlignmentHandler) calculationButton.getParent();
        parentAlignmentHandler.setComponentAlignment(calculationButton, Alignment.BOTTOM_LEFT);
        calculationButton.setIcon(VaadinIcons.CALC);
    }
    
    
    public final Button getCalculationButton() {
        return calculationButton;
    }
    
    protected void initNumbers() {
        numbers.addStyleName("");
        numbers.setEnabled(true);
        numbers.setVisible(true);
        numbers.setSpacing(true);
        numbers.setMargin(false);
        numbers.setWidth("100.0%");
        numbers.setHeight("100.0%");
        AbstractOrderedLayout parentAbstractOrderedLayout = (AbstractOrderedLayout) numbers.getParent();
        parentAbstractOrderedLayout.setExpandRatio(numbers, Double.valueOf(0.0).floatValue());
        AlignmentHandler parentAlignmentHandler = (AlignmentHandler) numbers.getParent();
        parentAlignmentHandler.setComponentAlignment(numbers, Alignment.TOP_LEFT);
    }
    
    public final HorizontalLayout getNumbers() {
        return numbers;
    }
    
    protected void initResultLabel() {
        resultLabel.setValue("");
        resultLabel.setContentMode(com.vaadin.shared.ui.ContentMode.TEXT);
        resultLabel.setCaption("");
        resultLabel.addStyleName(ValoTheme.LABEL_H1);
        resultLabel.setEnabled(true);
        resultLabel.setVisible(true);
        resultLabel.setWidth("100.0%");
        resultLabel.setHeight("100.0%");
        AbstractOrderedLayout parentAbstractOrderedLayout = (AbstractOrderedLayout) resultLabel.getParent();
        parentAbstractOrderedLayout.setExpandRatio(resultLabel, Double.valueOf(0.0).floatValue());
        AlignmentHandler parentAlignmentHandler = (AlignmentHandler) resultLabel.getParent();
        parentAlignmentHandler.setComponentAlignment(resultLabel, Alignment.TOP_LEFT);
    }
    
    
    public final Label getResultLabel() {
        return resultLabel;
    }
    
    protected void initRootCSS() {
        rootCSS.addStyleName("");
        rootCSS.setEnabled(true);
        rootCSS.setVisible(true);
        rootCSS.setSpacing(true);
        rootCSS.setMargin(true);
        rootCSS.setWidth("100.0%");
        rootCSS.setHeight("-1.0px");
    }
    
    public final VerticalLayout getRootCSS() {
        return rootCSS;
    }
}
