package de.mekaso.vaadin;

import java.math.BigDecimal;
import java.text.NumberFormat;
import java.util.Locale;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;

import com.vaadin.cdi.CDIView;
import com.vaadin.icons.VaadinIcons;
import com.vaadin.navigator.View;
import com.vaadin.navigator.ViewChangeListener.ViewChangeEvent;
import com.vaadin.ui.CustomComponent;
import com.vaadin.ui.Notification;

import de.mekaso.ejb.exceptions.DoesNotWorkException;
import de.mekaso.ejb.model.Operation;
import de.mekaso.ejb.model.OperationType;
import de.mekaso.ejb.service.CalculationService;

@CDIView("")
public class IndexView extends CustomComponent implements View {
	private static final long serialVersionUID = 1L;
	private Layout layout;
	
	@EJB(beanName="CalculationService", beanInterface = CalculationService.class)
	private CalculationService calculationService;
	
	@PostConstruct
	public void init() {
		this.layout = new Layout();
	}
	
	@Override
	public void enter(ViewChangeEvent event) {
		super.setCompositionRoot(this.layout.getRootCSS());
		this.layout.getOperationTypeComboBox().setItems(OperationType.values());
		this.layout.getOperationTypeComboBox().setValue(OperationType.Addition);
		addBehaviour();
	}
	
	private void addBehaviour() {
		this.layout.getCalculationButton().addClickListener(click -> {
			Operation operation = createOperation();
			try {
				BigDecimal result = this.calculationService.calculate(operation);
				NumberFormat nf = NumberFormat.getInstance(Locale.GERMANY);
				this.layout.getResultLabel().setValue(nf.format(result));
			} catch (DoesNotWorkException e) {
				this.layout.getResultLabel().setValue(null);
				Notification errorNotification = Notification.show(e.getMessage());
				errorNotification.setDelayMsec(5000);
				errorNotification.setIcon(VaadinIcons.WARNING);
			}
		});
	}

	private Operation createOperation() {
		String s1 = this.layout.getA().getValue();
		String s2 = this.layout.getB().getValue();
		BigDecimal a = new BigDecimal(s1);
		BigDecimal b = new BigDecimal(s2);
		OperationType ot = this.layout.getOperationTypeComboBox().getValue();
		return new Operation(a, b, ot);
	}
}
