package de.mekaso.vaadin;

import javax.inject.Inject;

import com.vaadin.annotations.Title;
import com.vaadin.cdi.CDINavigator;
import com.vaadin.cdi.CDIUI;
import com.vaadin.server.VaadinRequest;
import com.vaadin.ui.UI;

@CDIUI("")
@Title("A simple Vaadin Calculator")
public class CalculatorUI extends UI {
	private static final long serialVersionUID = 1L;
	@Inject
	private CDINavigator navigator;

	@Override
	protected void init(VaadinRequest request) {
		this.navigator.init(this, this);
	}

}
